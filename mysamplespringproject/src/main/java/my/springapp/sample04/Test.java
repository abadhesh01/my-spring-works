
// Author: Abadhesh Mishra [Employee Id: 8117322]

package my.springapp.sample04;

import my.springapp.model.Engineer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SuppressWarnings("all")
public class Test {
    public static void main(String[] args) {

        /*
         * About Code:
         * -----------
         * CI of a dependant object.
         */

        // Creating and configuring the information regarding the engineer.
        ApplicationContext context = new ClassPathXmlApplicationContext("Config04.xml");

        // Getting the information regarding the engineer.
        Engineer engineer = (Engineer) context.getBean("engineer");

        // Printing the information regarding the engineer.
        System.out.println("\n" + engineer);

        // Closing the application context to prevent the resource leak.
        ((ClassPathXmlApplicationContext) context).close();

        // Message for successful execution of the code.
        System.out.println("Execution successful.\n");
    }
}
