
// Author: Abadhesh Mishra [Employee Id: 8117322]

package my.springapp.sample13;

import my.springapp.model.BaseSampleTemplate;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SuppressWarnings("all")
public class Test {
    public static void main(String[] args) {

        /*
         * Observation:
         * ------------
         * DI by factory methods.
         * [Note:] A factory method is a method that returns an instance of a class.
         */

        System.out.println("\nConfiguring all the objects.....");
        // Configuring all the objects.....
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("FactoryCfg.xml");

        System.out.println("\nGetting all the objects.....");
        // Getting all the objects.....
        String prefix = new String("obj");
        for (int count = 1; count <= 7; count ++) {
            BaseSampleTemplate template = (BaseSampleTemplate) context.getBean(prefix + count);
            System.out.println(template);
        }

        System.out.println("\nClosing the context to prevent resource leak.....");
        // Closing the context to prevent resource leak.....
        context.close();

        // Message for successful execution.
        System.out.println("\nExecution successful!\n");
    }
}
