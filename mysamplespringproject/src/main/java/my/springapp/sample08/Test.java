
// Author: Abadhesh Mishra [Employee Id: 8117322]

package my.springapp.sample08;

import my.springapp.model.Employee;
import my.springapp.model.Engineer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SuppressWarnings("all")
public class Test {
    public static void main(String[] args) {

        // About code: Spring Bean Inheritance

        // Instantiating and configuring all the objects.
        ApplicationContext context = new ClassPathXmlApplicationContext("Config08.xml");

        // Getting all the employees.....
        System.out.println("\nGetting all the employees.....");
        System.out.printf("\n%-30s %s", "Employee Hashcode", "Employee Bean");
        System.out.printf("\n%-30s %s", "-----------------", "-------------");
        for (int count = 1; count <= 6; count ++) {
            Employee employee = (Employee) context.getBean("emp" + count);
            System.out.printf("\n%-30s %s", employee.hashCode(), employee);
        }

        // Getting all the engineers.....
        System.out.println("\n\nGetting all the engineers.....");
        for (int count = 1; count <= 3; count ++) {
            Engineer engineer = (Engineer) context.getBean("er" + count);
            System.out.printf("\n" + engineer);
            System.out.println("\nEmployee Hashcode: " + engineer.getInfo().hashCode());
            System.out.println("Engineer hashcode: " + engineer.hashCode());
        }

        // Closing the application context to prevent the resource leak.....
        System.out.println("\nClosing the application context to prevent the resource leak.....");
        ((ClassPathXmlApplicationContext) context).close();

        // Message for successful execution of the code.
        System.out.println("\nExecution successful!\n");
    }
}
