
// Author: Abadhesh Mishra [Employee Id: 8117322]

package my.springapp.sample09;

import my.springapp.model.Organization;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SuppressWarnings("all")
public class Test {
    public static void main(String[] args) {

        /*
         * About Code:
         * -----------
         * SI of a list of objects of both 'java.lang.String' type and user-defined type.
         */

        // Creating and configuring the information regarding the organization.
        ApplicationContext context = new ClassPathXmlApplicationContext("Config09.xml");

        // Getting the information regarding the organization.
        Organization organization = (Organization) context.getBean("org");

        // Printing the information regarding the organization.
        System.out.println("\n" + organization);

        // Closing the application context to prevent the resource leak.
        ((ClassPathXmlApplicationContext) context).close();

        // Message for successful execution.
        System.out.println("\nExecution Successful.\n");
    }
}
