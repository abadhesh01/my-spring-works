
// Author: Abadhesh Mishra [Employee Id: 8117322]

package my.springapp.sample12;

import my.springapp.model.Engineer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SuppressWarnings("all")
public class Test02 {
    public static void main(String[] args) {

        /*
         * Observation:
         * ------------
         * DI using autowiring by type of the dependency.
         */

        System.out.println("\nConfiguring and assembling the objects.....");
        // Configuring and assembling the objects.....
        ApplicationContext context = new ClassPathXmlApplicationContext("ConfigAutowire02.xml");

        System.out.println("\nGetting the assembled objects.....");
        // Getting the assembled objects.....
        Engineer engineer = (Engineer) context.getBean("er");
        System.out.println("\n" + engineer);

        System.out.println("\nClosing the context to prevent resource leak.....");
        // Closing the context to prevent resource leak.....
        ((ClassPathXmlApplicationContext) context).close();

        // Message for successful execution.
        System.out.println("\nExecution successful.\n");
    }
}
