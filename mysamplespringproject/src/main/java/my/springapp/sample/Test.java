
// Author: Abadhesh Mishra [Employee Id: 8117322]

package my.springapp.sample;

import my.springapp.model.Employee;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SuppressWarnings("all")
public class Test {
    public static void main(String[] args) {
        /*
         * Note:
         * -----
         * The interface "org.springframework.context.ApplicationContext" instance acts as an IoC (Inversion of Control) container.
         * The "AppConfig.xml" file contains the meta-data about the "my.springapp.model.Employee" class whose object needs
         * to be created, and it's fields' values. This file name was fetched into "org.springframework.context.support.ClassPathXmlApplicationContext"
         * class instance which is an implementation of "org.springframework.context.ApplicationContext" interface.
         */
        ApplicationContext context = new ClassPathXmlApplicationContext("AppConfig.xml");

        // Getting the "my.springapp.model.Employee" class object from IoC container.
        Employee employee = (Employee) context.getBean("myEmployee");

        // Printing the object info.
        System.out.println("\n" + employee);

        // Closing the IoC container (or context) to prevent resource leak.
        ((ClassPathXmlApplicationContext)context).close();

        // Message for successful execution.
        System.out.println("Execution successful.\n");
    }
}
