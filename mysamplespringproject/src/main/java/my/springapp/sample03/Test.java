
// Author: Abadhesh Mishra [Employee Id: 8117322]

package my.springapp.sample03;

import my.springapp.model.Employee;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("all")
public class Test {
    public static void main(String[] args) {

        /* Observation:
         * ------------
         * -> The CI (Constructor Injection) injects the values with respective suitable parameters.
         * -> If any arguement value is provided without type during CI, highest priority of arguement
         *    type is always of "java.lang.String" type, then of any primitive type.
         */

        // Creating and configuring all the employees.
        ApplicationContext context = new ClassPathXmlApplicationContext("Config03.xml");

        // Getting all the employees.
        List<Employee> employees = new LinkedList<>();
        for (int serialNumber = 1; serialNumber <= 15; serialNumber ++) {
            employees.add((Employee) context.getBean("employee" + serialNumber));
        }

        // Printing all the employees details.
        System.out.println("\nGetting all the employees.....\n");
        for (Employee employee: employees)
            System.out.println(employee);

        // Closing the context to prevent resource leak.
        ((ClassPathXmlApplicationContext)context).close();

        // Message for successful execution.
        System.out.println("Execution successful.\n");
    }
}
