
// Author: Abadhesh Mishra [Employee Id: 8117322]

package my.springapp.model;

@SuppressWarnings("all")
public class Engineer {

    // Fields
    private Employee info;
    private String type;
    private String position;
    private int tier;

    // Parameterized Constructor
    public Engineer(Employee info, String type, String position, int tier) {
        System.out.println("\nInstantiating \"" + getClass().getName() + "\" by parameterized constructor.");
        this.info = info;
        this.type = type;
        this.position = position;
        this.tier = tier;
    }

    // Default Constructor
    public Engineer() {
        System.out.println("\nInstantiating \"" + getClass().getName() + "\" by default constructor.");
    }

    // To String.
    @Override
    public String toString() {
        return "Engineer[" +
                "\ninfo=" + info +
                ",\ntype='" + type + '\'' +
                ",\nposition='" + position + '\'' +
                ",\ntier=" + tier +
                "\n" +
                ']';
    }

    // Getters and Setters.
    public Employee getInfo() {
        return info;
    }

    public void setInfo(Employee info) {
        System.out.println("Setting up the field \"info\" for the object \""
                + getClass().getName() + "@" + hashCode() + "\".");
        this.info = info;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        System.out.println("Setting up the field \"type\" for the object \""
                + getClass().getName() + "@" + hashCode() + "\".");
        this.type = type;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        System.out.println("Setting up the field \"position\" for the object \""
                + getClass().getName() + "@" + hashCode() + "\".");
        this.position = position;
    }

    public int getTier() {
        return tier;
    }

    public void setTier(int tier) {
        System.out.println("Setting up the field \"tier\" for the object \""
                + getClass().getName() + "@" + hashCode() + "\".");
        this.tier = tier;
    }
}

