
// Author: Abadhesh Mishra [Employee Id: 8117322]

package my.springapp.model;

import java.util.Map;
import java.util.Set;

@SuppressWarnings("all")
public class SoftwareCompany {

    // Fields
    private String name;
    private int estd;
    private int numberOfEmployees;
    private int numberOfProjects;
    private Map<String, String> projects;
    private Map<Long, Employee> employees;

    // Parameterized Constructor
    public SoftwareCompany(String name, int estd, int numberOfEmployees, int numberOfProjects, Map<String, String> projects, Map<Long, Employee> employees) {
        System.out.println("\nInstantiating \"" + getClass().getName() + "\" parameterized constructor.");
        System.out.println("Collection type of field 'projects': " + projects.getClass().getName());
        System.out.println("Collection type of field 'employees': " + employees.getClass().getName());
        this.name = name;
        this.estd = estd;
        //this.numberOfEmployees = numberOfEmployees;
        //this.numberOfProjects = numberOfProjects;
        this.projects = projects;
        this.employees = employees;
        this.numberOfEmployees = employees.size();
        this.numberOfProjects = projects.size();
    }

    // Default Constructor
    public SoftwareCompany() {
        System.out.println("\nInstantiating \"" + getClass().getName() + "\" default constructor.");
        /*
         * [Note:]
         * The below two lines are commented to prevent java.lang.NullpointerException at runtime.
         * The cause of the exception is "projects.getClass().getName()" and "employees.getClass().getName()"
         * is "null" while the default constructor i.e. this constructor is called.
         */
        //System.out.println("Collection type of field 'projects': " + projects.getClass().getName());
        //System.out.println("Collection type of field 'employees': " + employees.getClass().getName());
    }

    // Getters and Setters.
    public String getName() {
        return name;
    }

    public void setName(String name) {
        System.out.println("Setting up the field \"name\" for the object \""
                + getClass().getName() + "@" + hashCode() + "\".");
        this.name = name;
    }

    public int getEstd() {
        return estd;
    }

    public void setEstd(int estd) {
        System.out.println("Setting up the field \"estd\" for the object \""
                + getClass().getName() + "@" + hashCode() + "\".");
        this.estd = estd;
    }

    public int getNumberOfEmployees() {
        return numberOfEmployees;
    }

    public void setNumberOfEmployees(int numberOfEmployees) {
        System.out.println("Setting up the field \"numberOfEmployees\" for the object \""
                + getClass().getName() + "@" + hashCode() + "\".");
        //this.numberOfEmployees = numberOfEmployees;
        if (employees == null)
            return;
        this.numberOfEmployees = employees.size();
    }

    public int getNumberOfProjects() {
        return numberOfProjects;
    }

    public void setNumberOfProjects(int numberOfProjects) {
        System.out.println("Setting up the field \"numberOfProjects\" for the object \""
                + getClass().getName() + "@" + hashCode() + "\".");
        //this.numberOfProjects = numberOfProjects;
        if (projects == null)
            return;
        this.numberOfProjects = projects.size();
    }

    public Map<String, String> getProjects() {
        return projects;
    }

    public void setProjects(Map<String, String> projects) {
        System.out.println("Setting up the field \"projects\" for the object \""
                + getClass().getName() + "@" + hashCode() + "\".");
        this.projects = projects;
        System.out.println("Collection type of field 'projects': " + projects.getClass().getName());
    }

    public Map<Long, Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(Map<Long, Employee> employees) {
        System.out.println("Setting up the field \"employees\" for the object \""
                + getClass().getName() + "@" + hashCode() + "\".");
        this.employees = employees;
        System.out.println("Collection type of field 'employees': " + employees.getClass().getName());
    }

    // To String.
    @Override
    public String toString() {
        String text = "\nSoftwareCompany [" +
        "\nname='" + name + "'," +
        "\nestd='" + estd + "'," +
                "\nnumberOfEmployees='" + numberOfEmployees + "'," +
                "\nnumberOfProjects='" + numberOfProjects + "'," +
        "\nprojects={";

        Set<String> projectCodes = projects.keySet();
        Set<Long> employeeProjectCodes = employees.keySet();

        for (String code: projectCodes) {
            text += "\n \t (code='" + code + "', name='" + projects.get(code) + "'),";
        }

        text = text.substring(0, text.length() -1) + "},\nemployees={";

        for (Long employeeProjectCode: employeeProjectCodes) {
            text += "\n \t (employeeProjectCode='" + employeeProjectCode + "', " + employees.get(employeeProjectCode) + "),";
        }

        text = text.substring(0, text.length() -1) + "}\n]";

        return text;
    }
}
