
// Author: Abadhesh Mishra [Employee Id: 8117322]

package my.springapp.model;

@SuppressWarnings("all")
public class SampleTemplateBeta implements BaseSampleTemplate {

    // Private default constructor.
    private SampleTemplateBeta() {
        System.out.println("Instantiating object \""
                + getClass().getName() + "@" + hashCode()
                + "\" from \"private\" and \"default\" constructor.");
    }

    // Static factory method to return an object of type "my.springapp.model.SampleTemplateBeta".
    public static SampleTemplateBeta getSampleTemplateBeta() {
        String currentMethodInExecution = new Throwable().getStackTrace()[0].getMethodName();
        System.out.println("Current method in execution: " + currentMethodInExecution);
        return new SampleTemplateBeta();
    }

    // Static factory method to return an object of type "my.springapp.model.SampleTemplateAlpha".
    public static SampleTemplateAlpha getSampleTemplateAlphaFromSampleTemplateBeta() {
        String currentMethodInExecution = new Throwable().getStackTrace()[0].getMethodName();
        System.out.println("Current method in execution: " + currentMethodInExecution);
        return SampleTemplateAlpha.getSampleTemplateAlpha();
    }

}
