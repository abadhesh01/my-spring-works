
// Author: Abadhesh Mishra [Employee Id: 8117322]

package my.springapp.model;

import java.util.Collection;

@SuppressWarnings("all")
public class Organization {

    // Fields
    private String name;
    private int estd;
    private int numberOfEmployees;
    private Collection<String> addresses;
    private Collection<Employee> employees;

    // Parameterized Constructor
    public Organization(String name, int estd, int numberOfEmployees, Collection<String> addresses, Collection<Employee> employees) {
        System.out.println("\nInstantiating \"" + getClass().getName() + "\" by parameterized constructor.");
        System.out.println("Collection type of field 'addresses': " + addresses.getClass().getName());
        System.out.println("Collection type of field 'employees': " + employees.getClass().getName());
        this.name = name;
        this.estd = estd;
        //this.numberOfEmployees = numberOfEmployees;
        this.addresses = addresses;
        this.employees = employees;
        this.numberOfEmployees = employees.size();
    }

    // Default Constructor
    public Organization() {
        System.out.println("\nInstantiating \"" + getClass().getName() + "\" by default constructor.");
        /*
         * [Note:]
         * The below two lines are commented to prevent java.lang.NullpointerException at runtime.
         * The cause of the exception is "addresses.getClass().getName()" and "employees.getClass().getName()"
         * is "null" while the default constructor i.e. this constructor is called.
         */
        //System.out.println("Collection type of field 'addresses': " + addresses.getClass().getName());
        //System.out.println("Collection type of field 'employees': " + employees.getClass().getName());
    }

    // To String.
    @Override
    public String toString() {
        String text = "Organization[" +
                "\nname='" + name + '\'' +
                ",\nestd=" + estd +
                ",\nnumberOfEmployees=" + numberOfEmployees +
                ",\naddresses={";

        for (String address: addresses) {
            text += "\n \t {" + address + "},";
        }

        text = text.substring(0, text.length() -1) + "},\nemployees={";

        for (Employee employee: employees) {
            text += "\n \t " + employee + ",";
        }

        text = text.substring(0, text.length() -1) + "}\n]";

        return text;
    }

    // Getters and Setters.
    public String getName() {
        return name;
    }

    public void setName(String name) {
        System.out.println("Setting up the field \"name\" for the object \""
                + getClass().getName() + "@" + hashCode() + "\".");
        this.name = name;
    }

    public int getEstd() {
        return estd;
    }

    public void setEstd(int estd) {
        System.out.println("Setting up the field \"estd\" for the object \""
                + getClass().getName() + "@" + hashCode() + "\".");
        this.estd = estd;
    }

    public int getNumberOfEmployees() {
        return numberOfEmployees;
    }

    public void setNumberOfEmployees(int numberOfEmployees) {
        System.out.println("Setting up the field \"numberOfEmployees\" for the object \""
                + getClass().getName() + "@" + hashCode() + "\".");
        if (employees == null)
            return;
        this.numberOfEmployees = employees.size();
    }

    public Collection<String> getAddresses() {
        return addresses;
    }

    public void setAddresses(Collection<String> addresses) {
        System.out.println("Setting up the field \"addresses\" for the object \""
                + getClass().getName() + "@" + hashCode() + "\".");
        this.addresses = addresses;
        System.out.println("Collection type of field 'addresses': " + addresses.getClass().getName());
    }

    public Collection<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(Collection<Employee> employees) {
        System.out.println("Setting up the field \"employees\" for the object \""
                + getClass().getName() + "@" + hashCode() + "\".");
        this.employees = employees;
        System.out.println("Collection type of field 'employees': " + employees.getClass().getName());
    }
}
