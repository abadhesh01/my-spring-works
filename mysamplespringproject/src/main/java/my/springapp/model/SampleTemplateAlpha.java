
// Author: Abadhesh Mishra [Employee Id: 8117322]

package my.springapp.model;

@SuppressWarnings("all")
public class SampleTemplateAlpha implements BaseSampleTemplate {

    // Private default constructor.
    private SampleTemplateAlpha() {
        System.out.println("Instantiating object \""
                + getClass().getName() + "@" + hashCode()
                + "\" from \"private\" and \"default\" constructor.");
    }

    // Static factory method to return an object of type "my.springapp.model.SampleTemplateAlpha".
    public static SampleTemplateAlpha getSampleTemplateAlpha() {
        String currentMethodInExecution = new Throwable().getStackTrace()[0].getMethodName();
        System.out.println("Current method in execution: " + currentMethodInExecution);
        return new SampleTemplateAlpha();
    }

}
