
// Author: Abadhesh Mishra [Employee Id: 8117322]

package my.springapp.model;

@SuppressWarnings("all")
public interface BaseSampleTemplate {

    // Sample Default Method 1
    public default BaseSampleTemplate getAlpha() {
        return null;
    }

    // Sample Default Method 2
    public default BaseSampleTemplate getBeta() {
        return null;
    }

    // Sample Default Method 3
    public default BaseSampleTemplate getGamma() {
        return null;
    }
}
