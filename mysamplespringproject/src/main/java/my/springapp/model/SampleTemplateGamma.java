
// Author: Abadhesh Mishra [Employee Id: 8117322]

package my.springapp.model;

@SuppressWarnings("all")
public class SampleTemplateGamma implements BaseSampleTemplate {

    // Public default constructor.
    public SampleTemplateGamma() {
        System.out.println("Instantiating object \""
                + getClass().getName() + "@" + hashCode()
                + "\" from \"public\" and \"default\" constructor.");
    }

    // Static factory method to return an object of type "my.springapp.model.SampleTemplateGamma".
    public static SampleTemplateGamma getSampleTemplateGamma() {
        String currentMethodInExecution = new Throwable().getStackTrace()[0].getMethodName();
        System.out.println("Current method in execution: " + currentMethodInExecution);
        return new SampleTemplateGamma();
    }

    // Dynamic factory method to return an object of type "my.springapp.model.SampleTemplateAlpha".
    @Override
    public SampleTemplateAlpha getAlpha()
    {
        String currentMethodInExecution = new Throwable().getStackTrace()[0].getMethodName();
        System.out.println("Current method in execution: " + currentMethodInExecution);
        return SampleTemplateAlpha.getSampleTemplateAlpha();
    }

    // Dynamic factory method to return an object of type "my.springapp.model.SampleTemplateBeta".
    @Override
    public SampleTemplateBeta getBeta()
    {
        String currentMethodInExecution = new Throwable().getStackTrace()[0].getMethodName();
        System.out.println("Current method in execution: " + currentMethodInExecution);
        return SampleTemplateBeta.getSampleTemplateBeta();
    }

    // Dynamic factory method to return an object of type "my.springapp.model.SampleTemplateGamma".
    @Override
    public SampleTemplateGamma getGamma()
    {
        String currentMethodInExecution = new Throwable().getStackTrace()[0].getMethodName();
        System.out.println("Current method in execution: " + currentMethodInExecution);
        return this;
    }
}
