
// Author: Abadhesh Mishra [Employee Id: 8117322]

package my.springapp.model;

@SuppressWarnings("all")
public class Employee {

    // Fields
    private long id;
    private String firstName;
    private String lastName;
    private String job;

    // Employee count.
    private static int employeeCount;

    // Getters & Setters
    public long getId() {
        return id;
    }

    public void setId(long id) {
        System.out.print("Setting up the field \"id\" for the object \""
                + getClass().getName() + "@" + hashCode() + "\".");
        this.id = id;
        System.out.println("[id='" + this.id + "']");
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        System.out.print("Setting up the field \"firstName\" for the object \""
                + getClass().getName() + "@" + hashCode() + "\".");
        this.firstName = firstName;
        System.out.println("[firstName='" + this.firstName + "']");
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        System.out.print("Setting up the field \"lastName\" for the object \""
                + getClass().getName() + "@" + "\".");
        this.lastName = lastName;
        System.out.println("[lastName='" + this.lastName + "']");
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        System.out.print("Setting up the field \"job\" for the object \""
                + getClass().getName() + "@" + hashCode() + "\".");
        this.job = job;
        System.out.println( "[job='" + this.job + "']");
    }

    // Parameterized Constructors
    public Employee(long id, String firstName, String lastName, String job) {
        System.out.println("\n<<<Employee Count: " + (++ employeeCount) + ">>>");
        System.out.println("Instantiating \"" + getClass().getName() + "\" with fields: "
        + "\n[1] id" + "\n[2] firstName" + "\n[3] lastName" + "\n[4] job");
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.job = job;
        System.out.println("[id='" + this.id + "']");
        System.out.println("[firstName='" + this.firstName + "']");
        System.out.println("[lastName='" + this.lastName + "']");
        System.out.println("[job='" + this.job + "']");
    }

    public Employee(long id) {
        System.out.println("\n<<<Employee Count: " + (++ employeeCount) + ">>>");
        System.out.println("Instantiating \"" + getClass().getName() + "\" with field \"id\" only.");
        this.id = id;
        System.out.println("[id='" + this.id + "']");
    }

    public Employee(String firstName, String lastName) {
        System.out.println("\n<<<Employee Count: " + (++ employeeCount) + ">>>");
        System.out.println("Instantiating \"" + getClass().getName() + "\" with fields: "
                + "\n[1] firstName" + "\n[2] lastName");
        this.firstName = firstName;
        this.lastName = lastName;
        System.out.println("[firstName='" + this.firstName + "']");
        System.out.println("[lastName='" + this.lastName + "']");
    }

    public Employee(String job) {
        System.out.println("\n<<<Employee Count: " + (++ employeeCount) + ">>>");
        System.out.println("Instantiating \"" + getClass().getName() + "\" with field \"job\" only.");
        this.job = job;
        System.out.println("[job='" + this.job + "']");
    }

    // Default Constructor
    public Employee(){
        System.out.println("\n<<<Employee Count: " + (++ employeeCount) + ">>>");
        System.out.println("Instantiating \"" + getClass().getName() + "\".");
    }

    // To String.
    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", job='" + job + '\'' +
                '}';
    }
}
