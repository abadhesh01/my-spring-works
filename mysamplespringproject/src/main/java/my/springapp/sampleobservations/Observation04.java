
// Author: Abadhesh Mishra [Employee Id: 8117322]

package my.springapp.sampleobservations;

import my.springapp.model.Engineer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SuppressWarnings("all")
public class Observation04 {
    public static void main(String[] args) {

        System.out.println("\nConfiguring all the engineers.....");
        // Configuring all the engineers.....
        ApplicationContext context = new ClassPathXmlApplicationContext("Obscfg04.xml");

        System.out.println("\nGetting all the engineers.....");
        // Getting all the engineers.....
        String prefix = new String("er");
        for (int count = 1; count <= 2; count ++) {
            Engineer engineer = (Engineer) context.getBean(prefix + count);
            System.out.println("\n" + engineer);
        }

        System.out.println("\nClosing the context to prevent resource leak.....");
        // Closing the context to prevent resource leak.....
        ((ClassPathXmlApplicationContext) context).close();

        // Message for successful execution.
        System.out.println("\nExecution successful.\n");
    }
}
