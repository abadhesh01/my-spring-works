
// Author: Abadhesh Mishra [Employee Id: 8117322]

package my.springapp.sampleobservations;

import my.springapp.model.Engineer;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Observation02 {
    public static void main(String[] args) {


        System.out.println("\nConfiguring all the engineers.....");

        // Configuring all the engineers.....
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("Obscfg02.xml");

        System.out.println("\nGetting all the engineers.....");

        // Getting all the engineers.....
        for (int count = 1; count <= 2; count ++)
            System.out.println("\n" + (Engineer) context.getBean("er" + count));

        System.out.println("\nClosing the context to prevent resource leak.....");

        // Closing the context to prevent resource leak.....
        context.close();

        // Message for successful execution of the program.
        System.out.println("\nExecution Successful!\n");
    }
}
