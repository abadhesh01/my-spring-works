
// Author: Abadhesh Mishra [Employee Id: 8117322]

package my.springapp.sampleobservations;

import my.springapp.model.Employee;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Observation {
    public static void main(String[] args) {


        System.out.println("\nConfiguring all the employees.....");

        // Configuring all the employees.....
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("Obscfg.xml");

        System.out.println("\nGetting all the employees.....\n");

        // Getting all the employees.....
        for (int count = 1; count <= 5; count ++)
            System.out.println((Employee) context.getBean("emp" + count));

        System.out.println("\nClosing the context to prevent resource leak.....");

        // Closing the context to prevent resource leak.....
        context.close();

        // Message for successful execution of the program.
        System.out.println("\nExecution Successful!\n");
    }
}
