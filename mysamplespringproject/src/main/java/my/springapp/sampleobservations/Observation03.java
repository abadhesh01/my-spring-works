
// Author: Abadhesh Mishra [Employee Id: 8117322]

package my.springapp.sampleobservations;

import my.springapp.model.Employee;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SuppressWarnings("all")
public class Observation03 {
    public static void main(String[] args) {

        System.out.println("\nConfiguring all the employees.....");

        // Configuring all the employees.....
        ApplicationContext context = new ClassPathXmlApplicationContext("Obscfg03.xml");

        System.out.println("\nGetting all the employees.....\n");

        // Getting all the employees.....
        String employeePrefix = new String("emp");
        for (int count = 1; count <= 6; count ++) {
            Employee employee = (Employee) context.getBean(employeePrefix + count);
            System.out.println(employee);
        }

        System.out.println("\nClosing the context to prevent resource leak.....");

        // Closing the context to prevent resource leak.....
        ((ClassPathXmlApplicationContext) context).close();

        // Message for successful execution.
        System.out.println("\nExecution successful.\n");
    }
}
