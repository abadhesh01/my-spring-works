
// Author: Abadhesh Mishra [Employee Id: 8117322]

package my.springapp.sample02;

import my.springapp.model.Employee;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("all")
public class Test {
    public static void main(String[] args) {

        /*
         * Observation:
         * ------------
         * If any property of an object is not provided in the
         * configuration file, it will be initialized to its default
         * value.
         */

        // Creating and configuring all the employees.
        ApplicationContext context = new ClassPathXmlApplicationContext("MyAppConfig.xml");

        // Getting all the employees.
        List<Employee> myEmployees = new LinkedList<>();
        myEmployees.add((Employee) context.getBean("myEmployee"));
        myEmployees.add((Employee) context.getBean("myEmployee2"));
        myEmployees.add((Employee) context.getBean("myEmployee3"));
        myEmployees.add((Employee) context.getBean("myEmployee4"));

        // Printing all the employees' details.
        System.out.println("\nGetting all the employees.....\n");
        for (Employee employee: myEmployees)
            System.out.println(employee);

        // Closing the context to prevent resource leak.
        ((ClassPathXmlApplicationContext)context).close();

        // Message for successful execution.
        System.out.println("Execution successful.\n");
    }
}
