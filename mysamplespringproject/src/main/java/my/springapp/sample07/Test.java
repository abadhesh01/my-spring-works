
// Author: Abadhesh Mishra [Employee Id: 8117322]

package my.springapp.sample07;

import my.springapp.model.SoftwareCompany;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SuppressWarnings("all")
public class Test {
    public static void main(String[] args) {

        /*
         * About Code:
         * -----------
         * CI of a map of objects of both 'java.lang.String' type and user-defined type.
         */

        // Creating and configuring the information regarding the software company.
        ApplicationContext context = new ClassPathXmlApplicationContext("Config07.xml");

        // Getting the information regarding the software company.
        SoftwareCompany company = (SoftwareCompany) context.getBean("company");

        // Printing the information regarding the software company.
        System.out.println("\n" + company);

        // Closing the application context to prevent the resource leak.
        ((ClassPathXmlApplicationContext) context).close();

        // Message for successful execution of the code.
        System.out.println("Execution successful.\n");
    }
}
